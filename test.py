def my_mestre_field(I2, I4, I6, I10):
    k = Sequence([I2, I4, I6, I10]).universe()

    try:
        k = k.fraction_field()
    except (TypeError, AttributeError, NotImplementedError):
        pass

    return k


# Lauter, Yang
def my_mestre_conic_xyz(I2, I4, I6, I10):
    x = 8 * (1 + 20 * I4 / (I2 ** 2)) / 225

    y = 16 * (1 + 80 * I4 / (I2 ** 2) - 600 * I6 / (I2 ** 3)) / 3375

    z = -64 * (-10800000 * I10 / (I2 ** 5) - 9 - 700 * I4 / (I2 ** 2) + 3600 * I6 / (I2 ** 3) + 12400 * I4 ** 2 / (
            I2 ** 4) - 48000 * I4 * I6 / (I2 ** 5)) / 253125

    return x, y, z


def my_mestre_conic_L(x, y, z):
    L = Matrix([
        [x + 6 * y, 6 * x ** 2 + 2 * y, 2 * z],
        [6 * x ** 2 + 2 * y, 2 * z, 9 * x ** 3 + 4 * x * y + 6 * y ** 2],
        [2 * z, 9 * x ** 3 + 4 * x * y + 6 * y ** 2, 6 * x ** 2 * y + 2 * y ** 2 + 3 * x * z]
    ])

    try:
        L = L * L.denominator()  # clears the denominator
    except (AttributeError, TypeError):
        pass

    return L


def my_mestre_conic(I2, I4, I6, I10, invariants=False):
    r"""
    Return the conic equation from Mestre's algorithm for Igusa-Clebsch invariants.

    INPUT:
    - ``I2, I4, I6, I10`` - Igusa-Clebsch invariants
    - ``invariants`` - Boolean (default: False) returns Mestre invariants x,y,z

    Example::
        print my_mestre_conic(1,2,3,4)
        Projective Conic Curve over Rational Field defined by -2572155000*u^2 - 317736000*u*v + 1250755459200*v^2 + 2501510918400*u*w + 39276887040*v*w + 2736219686912*w^2
    Example finite field::
        print my_mestre_conic(GF(7)(10),GF(7)(1),GF(7)(2),GF(7)(3))
        Projective Conic Curve over Finite Field of size 7 defined by -2*u*v - v^2 - 2*u*w + 2*v*w - 3*w^2
    """
    k = my_mestre_field(I2, I4, I6, I10)

    x, y, z = my_mestre_conic_xyz(I2, I4, I6, I10)

    L = my_mestre_conic_L(x, y, z)

    conic = Conic(k, L, 'u,v,w')

    if invariants:
        return conic, x, y, z

    return conic


def my_mestre_curve_cjik(x, y, z):
    c111 = 12 * x * y - 2 * y / 3 - 4 * z
    c112 = -18 * x ** 3 - 12 * x * y - 36 * y ** 2 - 2 * z
    c113 = -9 * x ** 3 - 36 * x ** 2 * y - 4 * x * y - 6 * x * z - 18 * y ** 2
    c122 = c113
    c123 = -54 * x ** 4 - 36 * x ** 2 * y - 36 * x * y ** 2 - 6 * x * z - 4 * y ** 2 - 24 * y * z
    c133 = -27 * x ** 4 / 2 - 72 * x ** 3 * y - 6 * x ** 2 * y - 9 * x ** 2 * z - 39 * x * y ** 2 - \
           36 * y ** 3 - 2 * y * z
    c222 = -27 * x ** 4 - 18 * x ** 2 * y - 6 * x * y ** 2 - 8 * y ** 2 / 3 + 2 * y * z
    c223 = 9 * x ** 3 * y - 27 * x ** 2 * z + 6 * x * y ** 2 + 18 * y ** 3 - 8 * y * z
    c233 = -81 * x ** 5 / 2 - 27 * x ** 3 * y - 9 * x ** 2 * y ** 2 - 4 * x * y ** 2 + 3 * x * y * z - 6 * z ** 2
    c333 = 27 * x ** 4 * y / 2 - 27 * x ** 3 * z / 2 + 9 * x ** 2 * y ** 2 + 3 * x * y ** 3 - 6 * x * y * z + \
           4 * y ** 3 / 3 - 10 * y ** 2 * z

    return [c111, c112, c113, c122, c123, c133, c222, c223, c233, c333]


def my_mestre_curve_f(k, conic, cjik):
    t = k['t'].gen()

    parametrization = conic.parametrization(morphism=False)[0]

    [F1, F2, F3] = [p(t, 1) for p in parametrization]

    c111, c112, c113, c122, c123, c133, c222, c223, c233, c333 = cjik

    f = c111 * F1 ** 3 + c112 * F1 ** 2 * F2 + c113 * F1 ** 2 * F3 + c122 * F1 * F2 ** 2 + \
        c123 * F1 * F2 * F3 + c133 * F1 * F3 ** 2 + c222 * F2 ** 3 + c223 * F2 ** 2 * F3 + \
        c233 * F2 * F3 ** 2 + c333 * F3 ** 3

    return f


def my_mestre_curve_validate_conic(k, conic):
    if conic.has_rational_point():
        return True

    raise ValueError("No such curve exists over %s as there are no rational points on %s" % (k, conic))


def my_mestre_curve(I2, I4, I6, I10):
    r"""
    Returns a HEC for Igusa-Clebsch invariants ( g = 2, charateristic != 2, 3, 5 )

    INPUT:
    - ``I2, I4, I6, I10`` - Igusa-Clebsch invariants

    Example finite field::
        print my_mestre_curve(GF(13)(1),3,7,5)
        Hyperelliptic Curve over Finite Field of size 13 defined by y^2 = 8*x^5 + 5*x^4 + 5*x^2 + 9*x + 3

    Example number field::
        k = QuadraticField(353, 'a')
        curve = my_mestre_curve([21, 225/64, 22941/512, 1], reduced = false)
        f = k['x'](curve.hyperelliptic_polynomials()[0])
    """
    k = my_mestre_field(I2, I4, I6, I10)

    conic, x, y, z = my_mestre_conic(I2, I4, I6, I10, True)

    my_mestre_curve_validate_conic(k, conic)

    cjik = my_mestre_curve_cjik(x, y, z)

    f = my_mestre_curve_f(k, conic, cjik)

    try:
        f = f * f.denominator()
    except (AttributeError, TypeError):
        pass

    return HyperellipticCurve(f)
